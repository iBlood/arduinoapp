/*------------------------------------------------------------------------
  iBlood - Mereni saturace hemoglobinu arterialni krvi kyslikem
  ------------------------------------------------------------------------*/

// https://github.com/WorldFamousElectronics/PulseSensor_Amped_Arduino
#include "Adafruit_Thermal.h"

// https://github.com/codebendercc/arduino-library-files/blob/master/libraries/SoftwareSerial/SoftwareSerial.h
#include "SoftwareSerial.h"

/*-------------------------------
  === Promenne ===
  -------------------------------*/

/*-------------------------------
  === Nastaveni tiskarny ===
  -------------------------------*/
#define TIS_TX_PIN 6                                  // Zluty kabel z TTL logiky
#define TIS_RX_PIN 5                                  // Zeleny kabel z TTL logiky
SoftwareSerial TIS_Serial(TIS_RX_PIN, TIS_TX_PIN);    // Deklarace komunikacniho objektu
Adafruit_Thermal tiskarna(&TIS_Serial);               // Definice adresoveho prostoru pro tiskarnu

volatile int BPM;                                     // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                                  // holds the incoming raw data
volatile int IBI = 600;                               // int that holds the time interval between beats! Must be seeded! 
volatile boolean Pulse = false;                       // "True" when User's live heartbeat is detected. "False" when not a "live beat". 
volatile boolean QS = false;                          // becomes true when Arduoino finds a beat.
volatile int rate[10];                                // array to hold last ten IBI values
volatile unsigned long sampleCounter = 0;             // used to determine pulse timing
volatile unsigned long lastBeatTime = 0;              // used to find IBI
volatile int P =512;                                  // used to find peak in pulse wave, seeded
volatile int T = 512;                                 // used to find trough in pulse wave, seeded
volatile int thresh = 525;                            // used to find instant moment of heart beat, seeded
volatile int amp = 100;                               // used to hold amplitude of pulse waveform, seeded
volatile boolean firstBeat = true;                    // used to seed rate array so we startup with reasonable BPM
volatile boolean secondBeat = false;                  // used to seed rate array so we startup with reasonable BPM

int pulsePin = 1;                                     // Pulse Sensor purple wire connected to analog pin 0

/*-------------------------------
  === Bezici kod ===
  -------------------------------*/

void setup() {
  Serial.begin(115200);     // Inicializace serioveho portu
  TIS_Serial.begin(19200);  // Inicializace komunikacniho portu pres TTL
  tiskarna.begin();         // Spusteni TTL komunikace
  interruptSetup();         // sets up to read Pulse Sensor signal every 2mS 
}

void loop() {
  // cteni serioveho portu
  serialParser(); 
  sendDataToSerial('S', Signal);                // goes to sendDataToSerial function   
  if (QS == true){                              // A Heartbeat Was Found
                                                // BPM and IBI have been Determined
                                                // Quantified Self "QS" true when arduino finds a heartbeat
      sendDataToSerial('B',BPM);                // send heart rate with a 'B' prefix
      sendDataToSerial('Q',IBI);                // send time between beats with a 'Q' prefix   
      QS = false;                               // reset the Quantified Self flag for next time    
  }
delay(20);                                      //  take a break
}

/*-------------------------------
  === Cteni seriove komunikace ===
  -------------------------------*/

char* serialString()
{
  static char str[21];
  if (!Serial.available()) return NULL;
  delay(64);
  memset(str, 0, sizeof(str)); // clear str
  byte count = 0;
  while (Serial.available())
  {
    char c = Serial.read();
    if (c >= 32 && count < sizeof(str) - 1)
    {
      str[count] = c;
      count++;
    }
  }
  str[count] = '\0';
  return str;
}

/*-------------------------------
  === Zpracovani seriove komunikace ===
  -------------------------------*/
void serialParser() {
  static boolean needPrompt = true;
  char* userInput;
  if (needPrompt) needPrompt = false;
  userInput = serialString();
  if (userInput != NULL)
  {
    if (getValue(userInput, ';', 0) == "print") {
      int _max = getValue(userInput, ';', 1).toInt();
      int _min = getValue(userInput, ';', 2).toInt();
      int _cyc = getValue(userInput, ';', 3).toInt();
      int _avg = getValue(userInput, ';', 4).toInt();

      tiskHodnot(_max, _min, _cyc, _avg);
    }

    tiskarna.setSize('S');
    tiskarna.feed(2);

  }

  needPrompt = true;
}

/*-------------------------------
  === Rozdeleni retezce ===
  -------------------------------*/
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

/*-------------------------------
  === Tisk hodnot ===
  -------------------------------*/
void tiskHodnot(int valMax, int valMin, int valCyc, int valAvg) {
  String _max = String(valMax);
  String _min = String(valMin);
  String _avg = String(valAvg);
  String _cyc = String(valCyc);

  tiskarna.justify('C');
  tiskarna.setSize('L');
  tiskarna.println("iBlood");
  tiskarna.feed(1);

  tiskarna.setSize('S');
  tiskarna.justify('L');

  tiskarna.feed(1);

  tiskarna.println("---------- INFORMACE -----------");

  if (valCyc > 0) {
    tiskarna.println(" Maximalni tep: " + _max);
    tiskarna.println(" Minimalni tep: " + _min);
    tiskarna.println("  Prumerny tep: " + _avg);
    tiskarna.println("Mericich cyklu: " + _cyc);

    tiskarna.println("\n------------ POPIS -------------");
    if(valAvg > 180){
      tiskarna.println("Vas tep neni v poradku.");  
    }else{
      tiskarna.println("Vas tep je v naprostem poradku.");  
    }
    tiskarna.feed(1);
  }else{
    tiskarna.println("Bylo provedeno 0 mericich cyklu");
  }
  
  tiskarna.justify('C');
  tiskarna.inverseOn();
  tiskarna.println("\n   2016 (C) iBlood   ");
  tiskarna.inverseOff();

  tiskarna.setDefault();
  tiskarna.feed(2);
}

//  Sends Data to Pulse Sensor Processing App, Native Mac App, or Third-party Serial Readers. 
void sendDataToSerial(char symbol, int data ){
    Serial.print(symbol);
    Serial.println(data);                
}

void interruptSetup(){     
  // Initializes Timer2 to throw an interrupt every 2mS.
  TCCR2A = 0x02;     // DISABLE PWM ON DIGITAL PINS 3 AND 11, AND GO INTO CTC MODE
  TCCR2B = 0x06;     // DON'T FORCE COMPARE, 256 PRESCALER 
  OCR2A = 0X7C;      // SET THE TOP OF THE COUNT TO 124 FOR 500Hz SAMPLE RATE
  TIMSK2 = 0x02;     // ENABLE INTERRUPT ON MATCH BETWEEN TIMER2 AND OCR2A
  sei();             // MAKE SURE GLOBAL INTERRUPTS ARE ENABLED      
} 

// THIS IS THE TIMER 2 INTERRUPT SERVICE ROUTINE. 
// Timer 2 makes sure that we take a reading every 2 miliseconds
ISR(TIMER2_COMPA_vect){                         // triggered when Timer2 counts to 124
  cli();                                      // disable interrupts while we do this
  Signal = analogRead(pulsePin);              // read the Pulse Sensor 
  sampleCounter += 2;                         // keep track of the time in mS with this variable
  int N = sampleCounter - lastBeatTime;       // monitor the time since the last beat to avoid noise

    //  find the peak and trough of the pulse wave
  if(Signal < thresh && N > (IBI/5)*3){       // avoid dichrotic noise by waiting 3/5 of last IBI
    if (Signal < T){                        // T is the trough
      T = Signal;                         // keep track of lowest point in pulse wave 
    }
  }

  if(Signal > thresh && Signal > P){          // thresh condition helps avoid noise
    P = Signal;                             // P is the peak
  }                                        // keep track of highest point in pulse wave

  //  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
  // signal surges up in value every time there is a pulse
  if (N > 250){                                   // avoid high frequency noise
    if ( (Signal > thresh) && (Pulse == false) && (N > (IBI/5)*3) ){        
      Pulse = true;                               // set the Pulse flag when we think there is a pulse
      IBI = sampleCounter - lastBeatTime;         // measure time between beats in mS
      lastBeatTime = sampleCounter;               // keep track of time for next pulse

      if(secondBeat){                        // if this is the second beat, if secondBeat == TRUE
        secondBeat = false;                  // clear secondBeat flag
        for(int i=0; i<=9; i++){             // seed the running total to get a realisitic BPM at startup
          rate[i] = IBI;                      
        }
      }

      if(firstBeat){                         // if it's the first time we found a beat, if firstBeat == TRUE
        firstBeat = false;                   // clear firstBeat flag
        secondBeat = true;                   // set the second beat flag
        sei();                               // enable interrupts again
        return;                              // IBI value is unreliable so discard it
      }   


      // keep a running total of the last 10 IBI values
      word runningTotal = 0;                  // clear the runningTotal variable    

      for(int i=0; i<=8; i++){                // shift data in the rate array
        rate[i] = rate[i+1];                  // and drop the oldest IBI value 
        runningTotal += rate[i];              // add up the 9 oldest IBI values
      }

      rate[9] = IBI;                          // add the latest IBI to the rate array
      runningTotal += rate[9];                // add the latest IBI to runningTotal
      runningTotal /= 10;                     // average the last 10 IBI values 
      BPM = 60000/runningTotal;               // how many beats can fit into a minute? that's BPM!
      QS = true;                              // set Quantified Self flag 
      // QS FLAG IS NOT CLEARED INSIDE THIS ISR
    }                       
  }

  if (Signal < thresh && Pulse == true){   // when the values are going down, the beat is over
    Pulse = false;                         // reset the Pulse flag so we can do it again
    amp = P - T;                           // get amplitude of the pulse wave
    thresh = amp/2 + T;                    // set thresh at 50% of the amplitude
    P = thresh;                            // reset these for next time
    T = thresh;
  }

  if (N > 2500){                           // if 2.5 seconds go by without a beat
    thresh = 512;                          // set thresh default
    P = 512;                               // set P default
    T = 512;                               // set T default
    lastBeatTime = sampleCounter;          // bring the lastBeatTime up to date        
    firstBeat = true;                      // set these to avoid noise
    secondBeat = false;                    // when we get the heartbeat back
  }

  sei();                                   // enable interrupts when youre done!
}// end isr


